<?php
session_start();
var_dump($_SESSION);
/**
 * Сценарий:
 * Вводим адрес: "/proba.php?action=add_to_cart&id=1"
 * Вводим адрес: "/proba.php?action=add_to_cart&id=10"
 * Вводим адрес: "/proba.php?action=create_order" - получаем сообщение "создался заказ с товарами 1 и 10"
 *
 */
switch ($_GET['action']) {
    case 'add_to_cart':
        echo "Добавляем в корзину товар {$_GET['id']}";
        $_SESSION['cart'][]  = $_GET['id'];
        break;
    case 'create_order':
        $tovary = implode(", ", $_SESSION['cart']);
        echo "создался заказ с товарами {$tovary}";
        break;
}
